package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ThreadControllerTests {
    private Thread thread;
    private Integer threadId;
    private String title;
    private String slug;
    private String forum;
    private String message;
    private String name;
    private User user;
    private Vote vote;
    private ArrayList<Post> posts;

    @BeforeEach
    void setup() {
    	threadId = 134123421;
	title = "title";
    	slug = "slug";
    	message = "message";
    	forum = "forum";
    	name = "name";
        thread = new Thread(name, new Timestamp(0), forum, message, slug, title, 23);
        
        posts = new ArrayList<>();
	Post post1 = new Post();
	post1.setAuthor(name);
	Post post2 = new Post();
	post2.setAuthor(name);
	posts.add(post1);
	posts.add(post2);
	
	user = new User();
	user.setNickname(name);
	
	vote = new Vote(name, 2);
    }

    @Test
    @DisplayName("check ID")
    void checkIdSlugTest() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController threadController = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            assertEquals(thread, threadController.CheckIdOrSlug(Integer.toString(threadId)), "Id found");
        }
    }

    @Test
    @DisplayName("check Slug")
    void checkIdSlugTest() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController threadController = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(thread, threadController.CheckIdOrSlug(slug), "Slug found.");
        }
    }

    @Test
    @DisplayName("create Post")
    void createPostTest() {
        List<Post> s_posts = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController threadController = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(s_posts), threadController.createPost(slug, s_posts));
        }
    }

    @Test
    @DisplayName("get Post")
    void getPostTest() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController threadController = new ThreadController();

            threadMock.when(() -> ThreadDAO.getPosts(thread.getId(), 200, 2, null, false)).thenReturn(posts);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), threadController.Posts(slug, 200, 2, null, false));
        }
    }

    @Test
    @DisplayName("check thread change")
    void changeThreadTest() {
        Thread newThread = new Thread("aname", new Timestamp(1), "aforum", "atitle", "aslug", "amessage", 234567);
        newThread.setId(21);
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController threadController = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("aslug")).thenReturn(newThread);
            threadMock.when(() -> ThreadDAO.getThreadById(21)).thenReturn(thread);
            ResponseEntity threadChnResponse = threadController.change("aslug", thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadChnResponse, "Change succesfull");
        }
    }

    @Test
    @DisplayName("check thread info")
    void getThreadInformationTest() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController threadController = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            ResponseEntity threadInfResponse = threadController.info(slug);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadInfResponse, "Information succesfull");
        }
    }
    
    @Test
    @DisplayName("check vote failure")
    void createVotesTests() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
		mockedUserDAO.when(() -> UserDAO.Info(name)).thenReturn(user);
        	mockedThreadDAO.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
      	        mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);


                ThreadController threadController = new ThreadController();

                ResponseEntity threadInfoResponse = threadController.createVote(slug, vote);
	        Integer newVotes = 2 + vote.getVoice();
		
		//assertEquals(ResponseEntity.status(HttpStatus.CONFLICT), threadInfoResponse);
                //assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadInfoResponse, "Information succesfull");
            }
        }
    }

}

